/*
-- Query: SELECT * FROM upgrad8app.profiles
LIMIT 0, 1000

-- Date: 2017-10-08 01:43
*/
INSERT INTO `profiles` (`id`,`firstName`,`lastName`,`job_title`,`user_bio`,`createdAt`,`updatedAt`,`userId`) VALUES (1,'Alex','Kwon','Full Stack Web Developer (MEAN)','I’m a Florida-native now living in Silicon Valley, interested in entrepreneurship and creating scalable web apps with amazingly great UI/UX. I led the engineering team at Quizlet, building study tools that millions of students relied upon. I’ve also interned at Google and Yahoo! as a software engineer, and did lots of freelance website design while at the University of Florida.','2017-10-07 17:39:22','2017-10-07 17:39:22',NULL);
