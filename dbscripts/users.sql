/*
-- Query: SELECT * FROM upgrad8app.users
LIMIT 0, 1000

-- Date: 2017-10-08 01:32
*/
INSERT INTO `users` (`id`,`username`,`password`,`firstName`,`lastName`,`email`,`addressLine1`,`addressLine2`,`city`,`postcode`,`phone`,`reset_password_token`,`remember_created_at`,`sign_in_count`,`current_sign_in_at`,`last_sign_in_at`,`current_sign_in_ip`,`last_sign_in_ip`,`isAdmin`,`reset_password_sent_at`,`createdAt`,`updatedAt`) VALUES (1,'silverstonejustin@gmail.com','$2a$08$5d433PLFb6uFrHh3GSpRF.Q3kgZzQfSdVNzFZiTE7MjjS7wP07Hfy','Admin','Admin','silverstonejustin@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2017-10-07 17:25:53','2017-10-07 17:25:53');
INSERT INTO `users` (`id`,`username`,`password`,`firstName`,`lastName`,`email`,`addressLine1`,`addressLine2`,`city`,`postcode`,`phone`,`reset_password_token`,`remember_created_at`,`sign_in_count`,`current_sign_in_at`,`last_sign_in_at`,`current_sign_in_ip`,`last_sign_in_ip`,`isAdmin`,`reset_password_sent_at`,`createdAt`,`updatedAt`) VALUES (2,'png.justinfang@gmail.com','$2a$08$5d433PLFb6uFrHh3GSpRF.Q3kgZzQfSdVNzFZiTE7MjjS7wP07Hfy','Alex','Kwon','png.justinfang@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-07 17:25:53','2017-10-07 17:25:53');
