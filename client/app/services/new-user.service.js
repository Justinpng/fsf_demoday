
(function () {
    angular.module("upgrad8app")
        .service("NewUserAPI", [
            "$http",
            "$q",
            NewUserAPI
        ]);

    function NewUserAPI($http, $q) {
        var self = this;

        self.updateUserProfile = function (user){
            var defer = $q.defer();
            console.log(user);
            var userid  = user.id;
            var userTitle = user.userTitle;
            // console.log(user.userBio, '!!!')
            var userBio = user.userBio;
            $http.post("/api/users/" + user.id, user)
                .then(function(result){
                    defer.resolve(result);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

    }
})();