
(function () {
  angular
        .module("upgrad8app")
        .controller("LogoutCtrl", ["$state", "AuthFactory", "Flash", LogoutCtrl]);

    function LogoutCtrl($state, AuthFactory, Flash){
        var vm = this;

        vm.logout = function () {
            AuthFactory.logout()
                .then(function () {
                    $state.go("SignIn");
                }).catch(function () {
                console.error("Error logging on !");
            });
        };
    }
})();
