(function () {
    var app = angular
        .module("upgrad8app", [
            "ngFileUpload",
            "ui.router",
            "ngFlash",
            "ngSanitize",
            "ngProgress",
            "ngMessages",
            "data-table",
            "xeditable",
            "angulartics", 
            "angulartics.google.analytics",
            // "com.2fdevs.videogular",
        ]); 

    // bootstrap3 theme. Can be also 'bs2', 'default'
    app.run(function(editableOptions) {
        editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
      });
})();