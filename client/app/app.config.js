(function () {
    angular
        .module("upgrad8app")
        .config(upgrad8appConfig);
    upgrad8appConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function upgrad8appConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("SignIn", {
                url: "/signIn",
                views: {
                    "content": {
                        templateUrl: "../app/users/login.html"
                    }
                },
                controller: 'LoginCtrl',
                controllerAs: 'ctrl'
            })
            .state("SignUp", {
                url: "/signUp",
                views: {
                    "content": {
                        templateUrl: "../app/users/register.html"
                    }
                },
                controller: 'RegisterCtrl',
                controllerAs: 'ctrl'
            })
            .state("ResetPassword", {
                url: "/ResetPassword",
                views: {
                    "content": {
                        templateUrl: "../app/users/reset-password.html"
                    }
                },
                controller: 'ResetPasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state("ChangeNewpassword", {
                url: "/changeNewpassword?token",
                views: {
                    "content": {
                        templateUrl: "../app/users/change-new-password.html"
                    }
                },
                controller: 'ChangeNewPasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state("MyRewards", {
                url: "/MyRewards",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/myRewards.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        // return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'MyRewardsCtrl',
                controllerAs: 'ctrl'
            })
            .state("ClaimRewards", {
                url: "/ClaimRewards",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/claimRewards.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        // return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'ClaimRewardsCtrl',
                controllerAs: 'ctrl'
            })

            .state("MyFriends", {
                url: "/MyFriends",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/myFriends.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        // return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'MyFriendsCtrl',
                controllerAs: 'ctrl'
            })

            .state("MyAccount", {
                url: "/MyAccount",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        // return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'MyAccountCtrl',
                controllerAs: 'ctrl'
            })

            .state("ViewAlex", {
                url: "/viewAlex",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/demoprofile/viewAlex.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        // return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'viewProfileCtrl',
                controllerAs: 'ctrl'
            })

            .state("ViewLynette", {
                url: "/viewLynettex",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/demoprofile/viewLynette.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        // return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'viewProfileCtrl',
                controllerAs: 'ctrl'
            })

            .state("Upgrad8School", {
                url: "/upgrad8school",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/educators/upgrad8school.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        // return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'Upgrad8SchoolCtrl',
                controllerAs: 'ctrl'
            })


            .state("ChangePassword", {
                url: "/ChangePassword",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/changePassword.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'ChangePasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state('upgrad8', {
                url: '/upgrad8',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/upgrad8.html"
                    },
                    // "footer": {
                    //     templateUrl: "../app/protected/footer.html"
                    // }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        // console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            .state('submit', {
                url: '/submit',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/submit.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'submitCtrl',
                controllerAs: 'ctrl'
            })
            .state('badges', {
                url: '/badges',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/badges.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'GuestListCtrl',
                controllerAs: 'ctrl'
            })
            .state('educators', {
                url: '/educators',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/educators.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'EducatorsCtrl',
                controllerAs: 'ctrl'
            })
            .state('editProfile', {
                url: '/editProfile',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/editProfile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        // console.log("authenticated ?");
                        // console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'editProfileCtrl',
                controllerAs: 'ctrl'
            })
            .state('back', {
                url: '/back',
                templateUrl: './app/users/profile.html',
                controller: 'editProfileCtrl',
                controllerAs: 'ctrl'
            })
            .state('comments', {
                url: '/comments/:data',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/comment.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'CommentDetailCtrl',
                controllerAs: 'ctrl'
            })

        $urlRouterProvider.otherwise("/signIn");


    }
})();
