(function () {
    
        angular
            .module("upgrad8app")
            .controller("editProfileCtrl", ['UserAPI', '$scope', 'Flash', 'NewUserAPI', editProfileCtrl]);
    
        function editProfileCtrl(UserAPI, $scope, Flash, NewUserAPI) {
            var vm = this;
            vm.user = {};
            vm.sociallogins = [];
            vm.editProfile  = editProfile;

            initForm(vm);

            UserAPI.getLocalProfile().then(function (result) {
                console.log(result.data);
                vm.localProfile = result.data;
                
            });
    
            UserAPI.getAllSocialLoginsProfile().then(function (result) {
                vm.sociallogins = result.data;
            });

            function initForm(vm){
                // vm.user.curr_title = "";
                // vm.user.curr_bio = "";
                vm.user.userTitle = "";
                vm.user.userBio = "";
            }

            function editProfile(){
                console.log("Change Profile ...");
                console.log("Change Profile ...");
                console.log(vm.localProfile.id);
                console.log(vm.localProfile.userTitle);
                vm.user.id = vm.localProfile.id;
                vm.user.userTitle = vm.localProfile.userTitle;
                vm.user.userBio = vm.localProfile.userBio;
                console.log("Value of vm.user: ", vm.user);
                NewUserAPI.updateUserProfile(vm.user)
                    .then(function () {
                        // userTitle= vm.user.userTitle;
                        // userBio= vm.user.userBio;
                        // console.log("Why failing ?");
                        Flash.create('info', "Profile Updated.", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        // $state.go("/MyAccount");
                    }).catch(function (error) {
                        console.error("Update Error !");
                        console.error(error);
                    });
           }

        }
        

    })();
    
    