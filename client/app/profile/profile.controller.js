(function () {

    angular
        .module("upgrad8app")
        .controller("ProfileCtrl", ["UserAPI", "$scope", ProfileCtrl]);

    function ProfileCtrl(UserAPI, $http, $scope) {
        var vm = this;
        vm.sociallogins = [];
        UserAPI.getLocalProfile().then(function (result) {
            //console.log(result.data);
            vm.localProfile = result.data;
            
        });

        UserAPI.getAllSocialLoginsProfile().then(function (result) {
            vm.sociallogins = result.data;
        });
    }
    
    // app.controller('Ctrl', function($scope) {
    //     $scope.user = {
    //       name: 'awesome user'
    //     };  
    //   });
    
})();

