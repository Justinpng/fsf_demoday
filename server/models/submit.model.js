var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('submit', {
        userID: {
            type : Sequelize.STRING,
            allowNull: true
        },
        username: {
            type : Sequelize.STRING,
            allowNull: false
        },
        courseTitle: Sequelize.STRING,
        courseAcademy: Sequelize.STRING,
        courseType: Sequelize.STRING
    });
};