var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('user', {
        userID: {
            type : Sequelize.STRING,
            allowNull: true
        },
        username: {
            type : Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: true
        },
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        email: {
            type:Sequelize.STRING,
            allowNull: false
        },
        userScore: {
            type: Sequelize.STRING,
            allowNull: true
        },
        userTitle: {
            type: Sequelize.STRING,
            allowNull: true
        },
        userBio:  {
            type: Sequelize.TEXT,
            allowNull: true
        },
        reset_password_token: {
            type: Sequelize.STRING,
            allowNull: true
        },
        remember_created_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        sign_in_count: {
            type: Sequelize.INTEGER(11),
            allowNull: true
        },
        current_sign_in_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        last_sign_in_at: {
            type: Sequelize.DATE,
            allowNull: true
        },
        current_sign_in_ip: {
            type: Sequelize.STRING,
            allowNull: true
        },
        last_sign_in_ip: {
            type: Sequelize.STRING,
            allowNull: true
        },
        isAdmin: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
        reset_password_sent_at: {
            type: Sequelize.DATE,
            allowNull: true
        }
    });
};