'use strict';

var UserController = require("./api/user/user.controller");
// var ProfileController = require("./api/user/profile.controller");
// var AWSController = require("./api/aws/aws.controller");
var express = require("express");
var config = require("./config");

const API_USERS_URI = "/api/users";
// const API_PROFILE_URI = "/api/Profile";
// const API_AWS_URI = "/api/aws";
const HOME_PAGE = "/home.html#!/upgrad8";
const SIGNIN_PAGE = "/home.html#!/signIn";


module.exports = function(app, passport) {
    // Profile API
    // app.get(API_PROFILE_URI + '/image/:url', isAuthenticated, ProfileController.showImage);
    // app.post(API_PROFILE_URI, isAuthenticated, ProfileController.create);
    // app.get(API_PROFILE_URI + '/:id', isAuthenticated, ProfileController.get);
    // app.post(API_PROFILE_URI + '/:id', isAuthenticated, ProfileController.update);
    // app.delete(API_PROFILE_URI + '/:id', isAuthenticated, ProfileController.remove);

    // Users API
    app.get(API_USERS_URI, isAuthenticated, UserController.list);
    app.post(API_USERS_URI, isAuthenticated, UserController.create);
    app.get(API_USERS_URI + '/:id', isAuthenticated, UserController.get);
    app.post(API_USERS_URI + '/:id', isAuthenticated, UserController.update);
    app.delete(API_USERS_URI + '/:id', isAuthenticated, UserController.remove);
    app.get("/api/user/view-profile", isAuthenticated, UserController.profile);
    app.get("/api/user/social/profiles", isAuthenticated, UserController.profiles);

    app.get("/protected/", isAuthenticated, function(req, res) {
        if (req.user == null) {
            res.redirect(SIGNIN_PAGE);
        }
    })

    app.use(express.static(__dirname + "/../client/"));

    app.post("/change-password", isAuthenticated, UserController.changePasswd);

    app.get("/api/user/get-profile-token", UserController.profileToken);
    app.post("/api/user/change-passwordToken", UserController.changePasswdToken);

    app.post('/register', UserController.register);

    app.post("/login", passport.authenticate("local", {
        successRedirect: HOME_PAGE,
        failureRedirect: "/",
        failureFlash : true
    }));

    app.post("/reset-password", UserController.resetPasswd);

    app.get('/home', isAuthenticated, function(req, res) {
        res.redirect('..' + HOME_PAGE);
    });


    app.get("/oauth/google", passport.authenticate("google", {
        scope: ["email", "profile"]
    }));

    app.get("/oauth/google/callback", passport.authenticate("google", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE
    }));

    app.get("/oauth/facebook", passport.authenticate("facebook", {
        scope: ["email", "public_profile"]
    }));

    app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE,
        failureFlash: true
    }));



    app.get("/status/user", function(req, res) {
        var status = "";
        if (req.user) {
            status = req.user.email;
        }
        console.info("status of the user --> " + status);
        res.send(status).end();
    });

    app.get("/logout", function(req, res) {
        req.logout(); // clears the passport session
        req.session.destroy(); // destroys all session related data
        res.send(req.user).end();
    });


    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect(SIGNIN_PAGE);
    }
    app.use(function(req, res, next) {
        if (req.user == null) {
            res.redirect(SIGNIN_PAGE);
        }
        next();
    });

};