'use strict';

const domain_name = "http://ec2-52-221-191-137.ap-southeast-1.compute.amazonaws.com";

module.exports = {
    mysql: process.env.MYSQL_DATABASE_URL || /dbscripts/,
    domain_name: domain_name,
    aws: {
        id: "AKIAJAS2CYM5E75W4GFA",
        key: "LASkMJruTB0F41ti+v2r1x1RnrhldKgPIbuhK8PH",
        url: "https://nus-stackupday19.s3.amazonaws.com",
        bucket: "nus-stackupday19",
        region: "ap-southeast-1"
    },
    mailgun_key: "key-f6f5ef76817b15d64bd7c5de7c06d097", 
    mailgun_domain: "sandboxfec4a8eba0c243fbaaf491c17588ec22.mailgun.org",
    register_email: {
        from: "Upgrad8 Admin <noreply@upgrad8.com>",
        subject: "Welcome to Upgrad8",
        email_text: "Hello! Thank you for registering with Upgrad8. Have Fun Learning!"
    },
    reset_password_email: {
        from: "noreply@upgrad8.com",
        subject: "Request to Reset password for Upgrad8",
        email_text: "Hello! You recently requested a link to reset your password. Please set a new password on the following link !",
        email_content: "Hello! <br>You recently requested a link to reset your password. <br>Please set a new password on the following link <br>"
    },
    port: process.env.PORT || 3000,
    seed: process.env.SEED || false,
    GooglePlus_key: "552301539640-morchf1e4ig6q7gtfje4fl7l35i99uiv.apps.googleusercontent.com",
    GooglePlus_secret: "JbU_KW4qhXGv8eS7_j8p7f-t",
    GooglePlus_callback_url: domain_name + "/oauth/google/callback",
    Facebook_key: "1774952232780596",
    Facebook_secret: "bbd0cddf9a4605175f4d6fdfc951491e",
    Facebook_callback_url: domain_name + "/oauth/facebook/callback",
};