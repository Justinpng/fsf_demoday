'use strict';
const domain_name = "http://localhost:3000";

module.exports = {
    mysql: process.env.MYSQL_DATABASE_URL || "mysql://root:password@localhost/upgrad8app?reconnect=true",
    domain_name: domain_name,
    aws: {
        id: "AKIAJAS2CYM5E75W4GFA",
        key: "LASkMJruTB0F41ti+v2r1x1RnrhldKgPIbuhK8PH",
        url: "https://nus-stackupday19.s3.amazonaws.com",
        bucket: "nus-stackupday19",
        region: "ap-southeast-1"
    },
    mailgun_key: "key-f6f5ef76817b15d64bd7c5de7c06d097", 
    mailgun_domain: "sandboxfec4a8eba0c243fbaaf491c17588ec22.mailgun.org",
    register_email: {
        from: "Upgrad8 Admin <noreply@upgrad8.com>",
        subject: "Welcome to Upgrad8",
        email_text: "Hello! Thank you for registering with Upgrad8. Have Fun Learning!"
    },
    reset_password_email: {
        from: "noreply@upgrad8.com",
        subject: "Request to Reset password for Upgrad8",
        email_text: "Hello! You recently requested a link to reset your password. Please set a new password on the following link !",
        email_content: "Hello! <br>You recently requested a link to reset your password. <br>Please set a new password on the following link <br>"
    },
    port: 3000,
    seed: true,
    GooglePlus_key: process.env.GOOGLEPLUS_KEY || "552301539640-morchf1e4ig6q7gtfje4fl7l35i99uiv.apps.googleusercontent.com",
    GooglePlus_secret: process.env.GOOGLEPLUS_SECRET_KEY || "JbU_KW4qhXGv8eS7_j8p7f-t",
    GooglePlus_callback_url: process.env.GOOGLEPLUS_CALLBACK_URL || domain_name + "/oauth/google/callback",
    Facebook_key: process.env.FACEBOOK_KEY || "477613239279431",
    Facebook_secret: process.env.FACEBOOK_SECRET_KEY || "e85e35944f987ee3b91278561dcc51b2",
    Facebook_callback_url: process.env.FACEBOOK_CALLBACK_URL || domain_name + "/oauth/facebook/callback",

};