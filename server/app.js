var express = require("express");
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require("express-session");
var passport = require("passport");
var flash = require('connect-flash');
// var xeditable = require('angular-xeditable');

var config = require("./config");

var app = express();

app.use(flash());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Initialize session
app.use(session({
    secret: "upgrad8app-top-secret",
    resave: false,
    saveUninitialized: true
}));

// bootstrap3 theme. Can be also 'bs2', 'default'
// app.run(function(editableOptions) {
//     editableOptions.theme = 'bs3'; 
//   });

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

require('./auth')(app, passport);
require('./routes')(app, passport);

app.listen(config.port, function () {
    console.log("Server running at http://localhost:" + config.port);
});
