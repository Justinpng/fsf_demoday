var User = require("../../database").User;
var AuthProvider = require("../../database").AuthProvider;
// var Profile = require("../../database").Profile;
var config = require('../../config')

exports.get = function (req, res) {
    User
        .findById(req.params.id)
        .then(function (user) {

            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};


/**FOR LOCAL PROFILE */

exports.profile = function (req, res) {
    User.findOne({where: {email: req.user.email}})
        .then(function(result) {
            res.json(result);
        }).catch(function (err) {
        console.error(err);
        handleErr(res, err);
    });
};

exports.profileToken = function (req, res) {
    User.findOne({where: {reset_password_token: req.query.resetToken}})
        .then(function(result) {
            res.json(result);
        }).catch(function (err) {
            console.error(err);
            res.status(400).end();
            //console.error(err);
            //handleErr(res, err);
        });
};

/**FOR SOCIAL LOGIN PROFILES */
exports.profiles = function (req, res) {
    AuthProvider.findAll({where: {userId: req.user.id}})
        .then(function(result) {
            res.status(200).json(result);
        }).catch(function (err) {
        console.error(err);
        handleErr(res, err);
    });
};

exports.create = function (req, res) {
    User
        .create(req.body)
        .then(function (user) {
            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.update = function (req, res) {
    User
        .findById(req.params.id)
        .then(function (user) {

            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.remove = function (req, res) {
    User
        .findById(req.params.id)
        .then(function (user) {
            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}
