var bcrypt = require('bcryptjs');
var config = require("./config");
var database = require("./database");
var User = database.User;
// var Profile = database.Profile;

module.exports = function() {
    if (config.seed) {
        var hashpassword = bcrypt.hashSync("Aa@12345", bcrypt.genSaltSync(8), null);
        User
            .create({
                userID: '00001',
                username: "silverstonejustin@gmail.com",
                password: hashpassword,
                firstName: "Justin",
                lastName: "Png",
                email: "silverstonejustin@gmail.com",
                userScore: "",
                userTitle: "Administrator",
                userBio: "I’m a administrator for this site.",
                profilePic: "",
                isAdmin: true,
                // google: "http://google.com",
                // facebook: "http://facebook.com",
                // twitter: "http://twitter.com"
            })
            .then(function(user) {
                console.log(user);
            }).catch(function() {
                console.log("Error", arguments)
            })

        User
            .create({
                userID: '00001',
                username: "png.justinfang@gmail.com",
                password: hashpassword,
                firstName: "Justin",
                lastName: "Png",
                email: "png.justinfang@gmail.com",
                userScore: "1800",
                userTitle: "Technical Project Manager",
                userBio: "With a decade of experience working within creative and production field, Justin understood the agency setup and its operation inside-out. Justin is proficient with business development, client servicing, agency operations, project management as well as broadcast and production management. Justin has been able to come up with solutions that has been proven and recognised for consistent success in refining processes and procedures to enhance revenue performance. Fluent with Through-the-lines and digital campaigns.",
                profilePic: ""

                // google: "http://google.com",
                // facebook: "http://facebook.com",
                // twitter: "http://twitter.com"
            })

            // Profile
            // .create({
            //     userID: '00001',
            //     firstName: "Alex",
            //     lastName: "Kwon",
            //     userScore: "3200",
            //     userTitle: "Full Stack Web Developer",
            //     userBio: "I’m a Florida-native now living in Silicon Valley, interested in entrepreneurship and creating scalable web apps with amazingly great UI/UX. I’m leading product/engineering at Close.io, building a product people love and growing our SaaS business to be something great.Previously I led the engineering team at Quizlet, building study tools that millions of students relied upon. I’ve also interned at Google and Yahoo! as a software engineer, and did lots of freelance website design while at the University of Florida.",
            //     profilePic: ""
            // })


            .catch(function() {
                console.log("Error", arguments)
            })

    }
};